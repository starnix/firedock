#!/usr/bin/env bash

__usage__="$0 | [install|build|sound]\nRun without arguments for a firefox with no sound.\ninstall\t\tInstall the binary to /usr/local/bin\nbuild\t\tBuild the container needeed for sound support\nsound\t\tRun a firefox with support for sound"

__docker__=$(which docker)

if [ -z $__docker__ ]; then
    echo -e "Docker is sort of required...\n"
    exit 1
fi

function get_uid {
    echo $(id -u)
}

function get_bin_path {
  local SCRIPT_PATH="${BASH_SOURCE[0]}";
  if [ -h "${SCRIPT_PATH}" ]; then
      while [ -h "${SCRIPT_PATH}" ]; do
        SCRIPT_PATH=`readlink "${SCRIPT_PATH}"`;
      done
  fi
  pushd . > /dev/null
  cd `dirname ${SCRIPT_PATH}` > /dev/null
  SCRIPT_PATH=`pwd`;
  popd  > /dev/null
  echo "$SCRIPT_PATH"
}

function install {
  local install_path
  if [ -d ~/bin ]; then
      install_path=~/bin
  elif [ -d /usr/local/bin ]; then
      install_path=/usr/local/bin
  else
    install_path=/bin
  fi
  ln -s "$(get_bin_path)/firedock" $install_path/

}

function build {
    (set -x && cd $(get_bin_path) && \
	 cd ../ && \
	 _UID=$(get_uid) && \
	 sed -i "s/1000/${_UID}/g" Dockerfile pulse-client.conf && \
	 $__docker__ build --rm --force-rm --compress --no-cache -t firedock_$(get_uid) . && \
	 $__docker__ rmi $($__docker__ images | awk '/none/ {print $3}') && \
	 sed -i "s/${_UID}/1000/g" Dockerfile pulse-client.conf )
}  

function check_image {
    $__docker__ images | grep -q "firedock_$(get_uid)"
    return $?
}

case "${#}" in
    0)
	$__docker__ run --pull always --rm -d --shm-size 2g --volume=/tmp/.X11-unix:/tmp/.X11-unix:z -e DISPLAY=$DISPLAY starnix/firedock
	;;
    *)
	case $1 in
	    -h|--help|help)
		echo -e $__usage__
		exit 0
		;;
	    build)
		echo -e "Attempting build"
		build
		;;
	    install)
		install
		;;
	    sound)
		shift
		case "${#}" in
		    0)
			if check_image; then
			    $__docker__ run --rm -d --shm-size 2g --volume=/run/user/$(get_uid)/pulse:/run/user/$(get_uid)/pulse:z --volume=/tmp/.X11-unix:/tmp/.X11-unix:z -e DISPLAY=$DISPLAY firedock_$(get_uid)
			else
			    echo -e "Image for this user does not exist.  Please run 'firedock build' to build the image"
			    exit 1
			fi
			;;
		    *)
			if check_image; then
			    $__docker__ run --rm -d --shm-size 2g --volume=/run/user/$(get_uid)/pulse:/run/user/$(get_uid)/pulse:z --volume=/tmp/.X11-unix:/tmp/.X11-unix:z -e DISPLAY=$DISPLAY firedock_$(get_uid) "${@}"
			else
			    echo -e "Image for this user does not exist.  Please run 'firedock build' to build the image"
			    exit 1
			fi
			;;
		esac
		;;
	    *)
		$__docker__ --pull always run --rm -d --shm-size 2g --volume=/tmp/.X11-unix:/tmp/.X11-unix:z -e DISPLAY=$DISPLAY starnix/firedock "${@}"
		;;
	esac
	;;
esac
