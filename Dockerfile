FROM alpine:latest
MAINTAINER "David Rose david.rose@starnix.se"
RUN apk update && apk add alsa-plugins-pulse firefox-esr ttf-liberation ffmpeg
run adduser firefox -u 1000 -G audio -D
COPY pulse-client.conf /etc/pulse/client.conf
WORKDIR /home/firefox
USER firefox
ENTRYPOINT ["/usr/bin/firefox-esr"]
